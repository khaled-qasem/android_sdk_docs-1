# Gamiphy SDK

[![Version](https://img.shields.io/cocoapods/v/gamiphy.svg?style=flat)](https://cocoapods.org/pods/Gamiphy)
[![License](https://img.shields.io/cocoapods/l/gamiphy.svg?style=flat)](https://cocoapods.org/pods/Gamiphy)

## Inroduction 

Gamibot, is the loyality program that provide gamified user journey, with rewarding system, where users can get points by doing certine actions. and they 
can be rewarded for doing these actions. 

## Requirements

- Android Studio 3.1+

## Installation

gamiphy is available through [JitPack](https://jitpack.io/). To install
it, simply add the dependency for the Gamiphy SDK in your module (app-level) Gradle file (usually app/build.gradle):

```gradle
       // Todo : change this to gamiphy repo
       implementation 'com.github.khaled-qasem:GamiphySDK:0.0.1'
```

and make sure you have jitpack in your root-level (project-level) Gradle file (build.gradle), 
```gradle
   allprojects {
    repositories {
        google()
        jcenter()
        // add jitpack if it's not added
        maven { url 'https://jitpack.io' }
    }
}
```

## Getting started

Gamiphy SDK needs to be initialized in Application class, you can do that by calling the init methid as shown below, and pass some required data / parameters that 
you can get after you signup for an account at Gamiphy. kinldy note the initilize method below. 

```kotlin
      GamiphySDK.getInstance().init(botId,clientId,hmac)
```

## Showing the bot within your application

Gamibot can be triggered and shown in two methods. 

- If you are interested to use the widget that Gamiphy SDK provides, this widget will handle opening the bot within the web view.
you just need to add the widget button to your xml layout

```xml  
        <com.gamiphy.library.utils.GamiphyBotButton
        android:layout_width="70dp"
        android:layout_height="70dp" />
```

- If you are interested on having your own widget/button that will be repsonsible to open the bot, or you want to open the bot after a certin action. you can do so by calling the following method: 

```Kotlin
    GamiphySDK.getInstance().startWebView(context)
```

## Bot visitor flow 

Gamibot support the ability for the end users to navigate the different features available, without even being logged in. but whenever 
the users trying to perform the tasks / actions so they can get the points, Gamibot will encourage them to either login or signup to the application. 

You need to specify the Activity where the users can login / register in your application. you should implement GamiphyOnLoggedInEventListener by doing as the following: 

```Kotlin
GamiphySDK.getInstance().registerGamiphyOnLoggedInEventListener(object : GamiphyOnLoggedInEventListener {
            override fun onLoggedInEvent() {
                startActivity(Intent(applicationContext, LoginActivity::class.java))
            }
        }
        )
```

In login activity, after the user logged in, set user name and email and start gamiphy view
```kotlin
Todo: refactor setUserAuthRequest method and add callback with onSuccess and onError
 GamiphySDK.getInstance().setUserAuthRequest(
            UserAuthRequest(name,email)
        )
        gamiphySDK.startWebView(this)
```

Gamiphy SDK Listeners:

- GamiphyActionEventListener: this listener has onActionEvent method,this method called when an event triggered from the bot and returnes the action name.
```kotlin
GamiphySDK.getInstance().registerGamiphyActionEventListener(object : GamiphyActionEventListener {
            override fun onActionEvent(actionName: String) {
                Log.d(MainActivity::class.java.simpleName, "here is action name $actionName")
            }
        })
```

- GamiphyOnLoggedInEventListener: this listener has onLoggedInEvent method, this method called when the bot requires login for the user or the login button inside bot clicked.
```kotlin
GamiphySDK.getInstance().registerGamiphyOnLoggedInEventListener(object : GamiphyOnLoggedInEventListener {
            override fun onLoggedInEvent() {
                //make your action here, you may start login activity
                startActivity(Intent(applicationContext, LoginActivity::class.java))
            }
        }
        )
```

- GamiphyOnSignUpEventListener: this listener has onSignUpEventListener method, this method called when the bot requires register for the user or the signup button inside bot clicked.
```kotlin
 GamiphySDK.getInstance().registerGamiphyOnSignUpEventListener(object : GamiphyOnSignUpEventListener {
            override fun onSignUpEventListener() {
                startActivity(Intent(applicationContext, SignUpActivity::class.java))
            }
        }
        )
```

## Registering the users

As Gamibot is a loyality program that should be able to give points for the users, you can simply register your users for our SDK by calling this method. 

```kotlin
    Todo: refactor method name and pass email and name directly insted of UserAuthRequest and add callback with onSuccess and onError
    GamiphySDK.getInstance().registerUser(name,email)
    GamiphySDK.getInstance().setUserAuthRequest(UserAuthRequest(name,email))
```

you need to call this method in both cases the login / signup if you do instant login of your users after they login/signup. 


## Creating the tasks: 


You need to send the custom event actions whenever its done using the method trackEvent shown below.
This method take the event name label and mark it as done.

```kotlin
gamiphySDK.trackEvent(eventName,data)
```

